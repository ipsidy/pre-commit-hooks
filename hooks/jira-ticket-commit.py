import sys
import re

# RegEx for Jira ticket number
exp = re.compile('^[A-Za-z]*-[0-9]*:')

m = exp.match(sys.argv[1])

# check for a match
if m:
    exit(0)

# return error if no match is found
exit(1)
